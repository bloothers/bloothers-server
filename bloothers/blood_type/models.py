from py2neo.ogm import GraphObject, Property


class BloodType(GraphObject):
    """This class represents the Blood Type node on database.

    :param name: a string that is the name of the blood. It must be one of:
        'AB+', 'AB-', 'A+', 'A-', 'B+', 'B-', 'O+', 'O-'.
    """
    __primarykey__ = 'name'

    name = Property()

    def __init__(self, name):
        self.name = name
