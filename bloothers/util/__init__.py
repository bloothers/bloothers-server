from py2neo import NodeSelector
from .queries import populate_blood


class Neo4jUtils:
    """This class contains util methods to abstract some neo4j manipulation and
    configuration.

    :param graph: the :class:`Graph` instance to be manipulated.
    """

    def __init__(self, graph):
        self.graph = graph

    def create_constraints_if_not_exists(self, constaints_dict):
        """Method to help on creation of uniqueness constraints for many labels
        if they don't exists.

        :param constaints_dict: a dict containing the labels as key and
            a array of properties of the labels to be constrainted as value.
            For example:

            >>> util.create_constraints_if_not_exists(
                    {'label': ['property_one', 'property_two']})
        """
        for label in constaints_dict.keys():
            for constraint in constaints_dict[label]:
                constraints = self.graph.schema.get_uniqueness_constraints(
                    label=label)
                if constraint not in constraints:
                    self.graph.schema.create_uniqueness_constraint(
                        label=label, property_key=constraint)

    def create_blood_graph_if_not_exists(self):
        """Method to help on creation of Blood Types graph if it doesn't
        exist.
        """
        selector = NodeSelector(self.graph)
        selected = selector.select("BloodType")
        if len(list(selected)) == 0:
            self.graph.run(populate_blood.query)


def query2blood(blood, rh):
    """This function receives blood type without special characters and
    transforms it to the convetional bloodtype model with '+' or '-' special
    characters.

    :param blood: letter of the blood type.
    :param rh: extended plus or minus sign as string.
    :return: concatenated blood and rh in the blood type standard model.
    """
    if rh == 'plus':
        return blood + '+'

    elif rh == 'minus':
        return blood + '-'

    raise ValueError(
        'Not expected rh value. Expected values: `plus` or `minus`')
