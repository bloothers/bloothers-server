query = '''CREATE (:BloodType {name: "A+"})
CREATE (:BloodType {name: "A-"})
CREATE (:BloodType {name: "AB+"})
CREATE (:BloodType {name: "AB-"})
CREATE (:BloodType {name: "B+"})
CREATE (:BloodType {name: "B-"})
CREATE (:BloodType {name: "O+"})
CREATE (:BloodType {name: "O-"})
WITH 1 AS dummy

MATCH (ap:BloodType {name: "A+"})
MATCH (abp:BloodType {name: "AB+"})
CREATE (ap) - [:Donate] -> (ap)
CREATE (ap) - [:Donate] -> (abp)
WITH 1 AS dummy

MATCH (am:BloodType {name: "A-"})
MATCH (abp:BloodType {name: "AB+"})
MATCH (abm:BloodType {name: "AB-"})
MATCH (ap:BloodType {name: "A+"})
CREATE (am) - [:Donate] -> (am)
CREATE (am) - [:Donate] -> (abp)
CREATE (am) - [:Donate] -> (abm)
CREATE (am) - [:Donate] -> (ap)
WITH 1 AS dummy

MATCH (abp:BloodType {name: "AB+"})
CREATE (abp) - [:Donate] -> (abp)
WITH 1 AS dummy

MATCH (abm:BloodType {name: "AB-"})
MATCH (abp:BloodType {name: "AB+"})
CREATE (abm) - [:Donate] -> (abm)
CREATE (abm) - [:Donate] -> (abp)
WITH 1 AS dummy

MATCH (bp:BloodType {name: "B+"})
MATCH (abp:BloodType {name: "AB+"})
CREATE (bp) - [:Donate] -> (bp)
CREATE (bp) - [:Donate] -> (abp)
WITH 1 AS dummy

MATCH (bm:BloodType {name: "B-"})
MATCH (abp:BloodType {name: "AB+"})
MATCH (abm:BloodType {name: "AB-"})
MATCH (bp:BloodType {name: "B+"})
CREATE (bm) - [:Donate] -> (bm)
CREATE (bm) - [:Donate] -> (abp)
CREATE (bm) - [:Donate] -> (abm)
CREATE (bm) - [:Donate] -> (bp)
WITH 1 AS dummy

MATCH (op:BloodType {name: "O+"})
MATCH (abp:BloodType {name: "AB+"})
MATCH (bp:BloodType {name: "B+"})
MATCH (ap:BloodType {name: "A+"})
CREATE (op) - [:Donate] -> (op)
CREATE (op) - [:Donate] -> (abp)
CREATE (op) - [:Donate] -> (bp)
CREATE (op) - [:Donate] -> (ap)
WITH 1 AS dummy

MATCH (om:BloodType {name: "O-"})
MATCH (abp:BloodType {name: "AB+"})
MATCH (bp:BloodType {name: "B+"})
MATCH (ap:BloodType {name: "A+"})
MATCH (op:BloodType {name: "O+"})
MATCH (abm:BloodType {name: "AB-"})
MATCH (bm:BloodType {name: "B-"})
MATCH (am:BloodType {name: "A-"})
CREATE (om) - [:Donate] -> (om)
CREATE (om) - [:Donate] -> (abp)
CREATE (om) - [:Donate] -> (bp)
CREATE (om) - [:Donate] -> (ap)
CREATE (om) - [:Donate] -> (op)
CREATE (om) - [:Donate] -> (abm)
CREATE (om) - [:Donate] -> (bm)
CREATE (om) - [:Donate] -> (am)'''
