import logging

from flask import Flask
from flask_marshmallow import Marshmallow
from py2neo import Graph

from flask2neo import Flask2Neo
from .util import Neo4jUtils

ma = Marshmallow()
db = Flask2Neo()


def create_app(package_name,
               config='bloothers.config.Development',
               settings_override=None):
    """ A factory function which generates and configures a :class:`Flask` app
    based on default setup or on the given setup.

    :param package_name: application package name
    :param config: file containing app variable configurations.
        Defaults to bloothers.config.Development. If a file is defined in
        BLOOTHERS_SETTINGS environment variable, this file will override the
        default configuration.
    :param settings_override: a dictionary of settings to override. Defaults to
        None.
    :return app: :class:`Flask` application instance configured with common
        functionality for the platform.
    """

    app = Flask(package_name, instance_relative_config=True)

    app.config.from_object(config)
    app.config.from_envvar('BLOOTHERS_SETTINGS', silent=True)
    app.config.from_object(settings_override)

    with app.app_context():
        ma.init_app(app)
        _init_neo4j(app)
        _register_blueprints(app)
        _create_logger(app)

    return app


def _init_neo4j(app):
    """This function initializes correclty the Flask2Neo extension on
    :class:`Flask`, creates the needed database constraints and the Blood Types
    graph if it doesn't exist yet.

    :param app: the :class:`Flask` instance to initialize the extension.
    """

    db.init_app(app)

    utils = Neo4jUtils(db.graph)
    utils.create_constraints_if_not_exists({
        'Donor': ['uid', 'donor_id'],
        'BloodType': ['name']
    })
    utils.create_blood_graph_if_not_exists()


def _create_logger(app):
    """"This function creates and configures the application logger using
    :class:`Flask` app configuration variables.

    :param app: :class:`Flask` application instance.
    """
    handler = logging.FileHandler(app.config['LOGGING_LOCATION'])
    handler.setLevel(app.config['LOGGING_LEVEL'])
    formatter = logging.Formatter(app.config['LOGGING_FORMAT'])
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)


def _register_blueprints(app):
    """Registers all the application's blueprints on a given :class:`Flask`
    application instance.

    :param app: :class:`Flask` application instance.
    """
    from bloothers.donor.routes import donor
    from bloothers.errors.handlers import errors

    app.register_blueprint(donor)
    app.register_blueprint(errors)
