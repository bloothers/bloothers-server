import os
import logging


class BaseConfig:
    """Base Flask config variables."""

    DEBUG = False
    NEO4J_HOST = os.getenv('NEO4J_HOST', 'bolt://localhost:7687')
    NEO4J_USER = os.getenv('NEO4J_USER', 'neo4j')
    NEO4J_PASSWORD = os.getenv('NEO4J_PASSWORD', 'bl00th3r5')
    LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    LOGGING_LOCATION = 'bloothers.log'
    LOGGING_LEVEL = logging.DEBUG


class Test(BaseConfig):
    """Test Flask config variables."""

    NEO4J_HOST = os.getenv('NEO4J_HOST', 'bolt://localhost:7867')
    NEO4J_USER = os.getenv('NEO4J_USER', 'neo4j')
    NEO4J_PASSWORD = os.getenv('NEO4J_PASSWORD', 'bl00th3r5')
    DEBUG = True
    TESTING = True


class Development(BaseConfig):
    """Development Flask config variables."""

    DEBUG = True


class Production(BaseConfig):
    """Production Flask config variables."""

    DEBUG = False
