from flask import request, Blueprint, jsonify, Response
import jsonpatch

from .models import Donor, donor_schema
from ..util import query2blood
from . import graph

donor = Blueprint('donor', __name__, url_prefix='/donors')


@donor.route('/', methods=['GET'])
def get_donor():
    """Receives an HTTP package via GET method with filters, quantity and
    pagination parameters to be used in search and returns class:`Donor`s
    list based on these parameters.

    :param blood_type: blood type which will be filtetered in search.
    :param page: result number page of class:`Donor`s search to be returned.
    :param limit: number of :class:`Donor`s to be returned per page.

    :return donors: a list of :class:`Donor`s filtered and delimited by the
    parameters.
    """
    limit = request.args.get('limit')
    page = request.args.get('page')

    blood = request.args.get('blood')
    rh = request.args.get('rh')

    if blood and rh:
        blood_type = query2blood(blood, rh)
        if limit and page:
            return donor_schema.jsonify(
                graph.get_donors_by_blood(blood_type, int(limit), int(page)),
                many=True)
        if limit:
            return donor_schema.jsonify(
                graph.get_donors_by_blood(blood_type, int(limit)), many=True)
        if page:
            return donor_schema.jsonify(
                graph.get_donors_by_blood(blood_type, int(page)), many=True)
        return donor_schema.jsonify(
            graph.get_donors_by_blood(blood_type), many=True)

    if limit and page:
        return donor_schema.jsonify(
            graph.get_donors(int(limit), int(page)), many=True)
    if limit:
        return donor_schema.jsonify(
            graph.get_donors(int(limit)), many=True)
    if page:
        return donor_schema.jsonify(graph.get_donors(int(page)), many=True)
    return donor_schema.jsonify(graph.get_donors(), many=True)


@donor.route('/<int:donor_id>', methods=['GET'])
def get_donor_by_id(donor_id):
    """Receives an HTTP package via GET method and an id variable and returns a
    :class:`Donor` with matching id.

    :param donor_id: id of the :class:`Donor` to be returned.
    :return donor: :class:`Donor` who has the same id as the received
        parameter.
    """
    return donor_schema.jsonify(graph.get_donor(donor_id))


@donor.route('/', methods=['POST'])
def create_donor():
    """Receives an HTTP package via POST method with a JSON-Like
    :class:`Donor` in its body and saves the :class:`Donor` into the
    database with an generated id.

    :return donor: the created :class:`Donor` instance with id.
    """
    received_donor = donor_schema.load(request.get_json()).data
    graph.add_donor(received_donor)
    return donor_schema.jsonify(received_donor), 201


@donor.route('/<int:donor_id>', methods=['PUT'])
def update_donor(donor_id):
    """Receives an HTTP package via PUT method with a Json-Like
    :class:`Donor` in its body and updates the :class:`Donor` in database
    with the new attributes.

    :param donor_id: Id of the donor which will be updated.
    :return: 204 http code if the action was completed.
    """

    updated_donor = donor_schema.load(request.get_json()).data
    graph.update_donor(donor_id, updated_donor)

    return ('', 204)


@donor.route('/<int:donor_id>', methods=['PATCH'])
def patch_donor(donor_id):
    """Receives an HTTP package via PATCH method with a Json-Like JSON Patch in
    its body and updates the :class:`Donor` with the changes described on the
    patch.

    :param donor_id: id of Donor which will be updated.
    :return: 204 http code if the action was completed.
    """
    patch = request.get_json()
    donor = graph.get_donor(donor_id)
    new_donor = donor_schema.load(
        jsonpatch.apply_patch(vars(donor), patch)).data
    graph.update_donor(donor_id, new_donor)
    return ('', 204)


@donor.route('/<int:donor_id>', methods=['DELETE'])
def delete_donor(donor_id):
    """Receives an HTTP package via DELETE method and delete the
    :class:`Donor` who has the given id.

    :param id: id of the Donor which will be deleted.
    :return: 204 http code if the action was completed.
    """

    graph.delete_donor(donor_id)
    return ('', 204)
