from marshmallow import post_load, fields
from py2neo.ogm import GraphObject, Property, RelatedTo
from py2neo import Cursor
from datetime import datetime

from bloothers import ma
from ..blood_type.models import BloodType


class Donor(GraphObject):
    """Class that represents Donor user.

    :param donor_id: a number which is the unique identifier of Donor user.
    :param uid: Donor identifer from firebase.
    :param latitude: Donor's latitude of geographic location.
    :param longitude: Donor's longitude of geographic location.
    :param donation_range: allowed distance between Donor's location and
        donation place.
    :param blood_type: donor's blood type.
    :param last_donation: date of the last blood donation made my the Donor.
    """

    __primarykey__ = 'donor_id'

    donor_id = Property()
    latitude = Property()
    longitude = Property()
    donation_range = Property()
    _last_donation = Property()
    uid = Property()
    blood_type = RelatedTo('BloodType', 'Has')

    def __init__(self,
                 latitude,
                 longitude,
                 donation_range,
                 blood_type,
                 last_donation=None,
                 donor_id=None,
                 uid=None):

        self.latitude = latitude
        self.longitude = longitude
        self.donation_range = donation_range
        self.last_donation = last_donation
        self.donor_id = donor_id
        self.uid = uid
        self.blood_type.add(blood_type)

    def __repr__(self):
        return ('Donor (donor_id=%d, uid=%s, latitude=%f, longitude=%f,'
                ' donation_range=%d, blood_type=%s, last_donation=%s)') % (
                    self.donor_id, self.uid, self.latitude, self.longitude,
                    self.donation_range, self.blood_label, self.last_donation)

    @property
    def last_donation(self):
        if self._last_donation:
            return datetime.strptime(self._last_donation, '%Y-%m-%d')
        return None

    @last_donation.setter
    def last_donation(self, value):
        if value:
            self._last_donation = value.isoformat()
        else:
            self._last_donation = None

    @property
    def blood_label(self):
        if len(self.blood_type) > 0:
            for b in self.blood_type:
                return b.name
        return None

    @staticmethod
    def select_by_blood(graph, blood_type, limit=25, page=1):
        """This method searches and returns from database all the Donors that
        have the given blood type with a limit of results per page.

        :param blood_type: a string containing the blood type to be filtered.
        :param limit: an int representing the number of results per page.
            The default limit is 25.
        :param page: an int representing the result page that will be returned.
            The default page number is 1.
        :return: a list with registered Donors that have the given blood
            type, ordered by uid.
        """

        query = '''MATCH (d:Donor) - [:Has] -> (b:BloodType {name: {blood}})
        RETURN d, b ORDER BY d.donor_id
        SKIP {number_skips} LIMIT {results_number}'''

        skip = limit * (page - 1)
        result_set = graph.run(
            query, blood=blood_type, number_skips=skip,
            results_number=limit).data()
        return map(__class__._convert_donor, result_set)

    @staticmethod
    def _convert_donor(result_row):
        """This method get an item from a py2neo :class:`Cursor` and creates
        a :class:`Donor` based on it.

        :param result_row: a py2neo :class:`Cursor` item that represents a
            Donor.
        :return: the :class:`Donor` containing the informations given on the
            result_row.
        """
        donor_dict = dict(result_row['d'])
        blood_dict = dict(result_row['b'])
        donor_dict['blood_type'] = BloodType(**blood_dict)
        return Donor(**donor_dict)


class DonorSchema(ma.Schema):

    donor_id = fields.Int()
    latitude = fields.Float()
    longitude = fields.Float()
    donation_range = fields.Float()
    last_donation = fields.Date(allow_none=True)
    blood_type = fields.Method('get_blood_type', deserialize='load_blood_type')

    def get_blood_type(self, obj):
        return obj.blood_label

    def load_blood_type(self, value):
        return BloodType(value)

    @post_load
    def make_object(self, data):
        return Donor(**data)


donor_schema = DonorSchema()
