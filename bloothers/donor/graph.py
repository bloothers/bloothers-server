from .. import db
from .models import Donor


def add_donor(donor):
    """This function creates a :class:`Donor` on graph database and connects
    him to his respective blood type.

    :param donor: the :class:`Donor` that will be persisted.
    """

    db.graph.create(donor)


def get_donor(donor_id):
    """This function gets a :class:`Donor` on database that has the
    given id.

    :param donor_id: the :class:`Donor` id.
    :return: the :class:`Donor` that have the given id.
    """

    donor = Donor.select(db.graph, donor_id).first()
    return donor


def get_donors(limit=25, page=1):
    """This function returns a list with persisted Donors with
    a limit of results per page.

    :param limit: an int representing the number of results per page.
        The default limit is 25.
    :param page: an int representing the result page that will be returned.
        The default page number is 1.
    :return: a list with registered Donors, ordered by donor_id.
    """
    skip = limit * (page - 1)
    donors_selection = Donor.select(
        db.graph).limit(limit).skip(skip).order_by("_.donor_id")
    donors = list(donors_selection)
    return donors


def get_donors_by_blood(blood_type, limit=25, page=1):
    """This function returns a list of registered Donors
    that have the given blood type with a limit of results per
    page.

    :param blood_type: a string containing the blood type to be filtered.
    :param limit: an int representing the number of results per page.
        The default limit is 25.
    :param page: an int representing the result page that will be returned.
        The default page number is 1.
    :return: a list with registered Donors that have the given blood type,
        ordered by uid.
    """
    return Donor.select_by_blood(db.graph, blood_type, limit, page)


def update_donor(old_donor_id, new_donor):
    """This function updates the attributes of an registered Donor.

    :param old_donor_id: an int containing the identifier of the Donor that
        will be updated.
    :param new_donor: a :class:`Donor` with the attributes updates to put
        on database.
    """
    new_donor.donor_id = old_donor_id
    db.graph.push(new_donor)


def delete_donor(donor_id):
    """This function deletes the Donor with the given id.

    :param donor_id: an int containing the id of the Donor that will be
        deleted.
    """
    donor = Donor.select(db.graph, donor_id).first()
    db.graph.delete(donor)
