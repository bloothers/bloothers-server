import traceback

from flask import Blueprint, jsonify
from flask import current_app as app

from . import exceptions

errors = Blueprint('errors', __name__)


@errors.app_errorhandler(exceptions.BloothersError)
def handle_error(error):
    """Handles application :class:`BloothersError`s by transforming the message
    and the status code error into json.

    :param error: error to be handled and returned.

    :return error: json object with the type error and the error message.
    """
    app.logger.error(traceback.format_exc())

    response = {
        'error': {
            'type': error.__class__.__name__,
            'message': error.message
        }
    }
    return jsonify(response), error.status_code


@errors.app_errorhandler(Exception)
def handle_unexpected_error(error):
    """Handles unexpected errors in the applicantion.

    :param error: unexpected error to be handled and returned.

    :return error: json object with the type error and the error message.
    """
    app.logger.error(traceback.format_exc())

    status_code = 500
    response = {
        'error': {
            'type': 'UnexpectedException',
            'message': 'An unexpected error has occurred.'
        }
    }

    return jsonify(response), status_code
