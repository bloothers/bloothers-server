class BloothersError(Exception):
    """Base class for bloothers exceptions

    :param status_code: http status code that will be set in response.
    :param message: exception description message.
    """
    status_code = 500

    def __init__(self, status_code=500, message="Internal Server Error"):
        Exception.__init__(self)
        self.status_code = status_code
        self.message = message


class ResourceAlreadyExists(BloothersError):
    """It's raised when there is attemption to register a existing resource
    in the database.

    :param status_code: http status code that will be set in response.
    :param message: exception description message.
    """

    def __init__(self, status_code=409, message="Resource already exists"):
        BloothersError.__init__(self, status_code, message)


class InvalidQueryParameterValue(BloothersError):
    """It's raised when an invalid value was specified for one of the query
    parameters in the request.

    :param status_code: http status code that will be set in response.
    :param message: exception description message.
    """

    def __init__(self,
                 status_code=400,
                 message="Invalid Query Parameter Value"):
        BloothersError.__init__(self, status_code, message)


class ResourceDoesntExist(BloothersError):
    """It's raised when there is attempt to manipulate a nonexistent resource.

    :param status_code: http status code that will be set in response.
    :param message: exception description message.
    """

    def __init__(self, status_code=404, message="Resource Doesn't Exist"):
        BloothersError.__init__(self, status_code, message)


class InvalidData(BloothersError):
    """It's raised when there is a request with an invalid data.

    :param status_code: http status code that will be set in response.
    :param message: exception description message.
    """

    def __init__(self, status_code=400, message="Invalid Data"):
        BloothersError.__init__(self, status_code, message)
