import unittest
import json
from parametrized import parametrized

from . import InstrumentedTest, DONOR_BASE_PATH
from bloothers.blood_type.models import BloodType


class DonorTestCase(unittest.TestCase, InstrumentedTest):
    def setUp(self):
        self.setUpIntegrationTests()

    def test_donor_creation(self):
        for i in range(8):
            donor = self.make_donor(10, 10, 10, self.blood_types[i], i,
                                        '2017-02-15T00:00:00')
            resp = self.app.post(
                DONOR_BASE_PATH,
                data=json.dumps(donor),
                content_type='application/json')

            donor_response = resp.json
            registered_donor = self.get_donor(i)

            self.assertEqual(resp.headers['content-type'], 'application/json')
            self.assertEqual(resp.status_code, 201)
            self.assertEqual(donor_response, donor)
            self.assertEqual(registered_donor, donor)

            response = self.app.get(DONOR_BASE_PATH)
            self.assertEqual(len(response.json), i + 1)

    def test_donor_deletion(self):
        for i in range(8):
            donor = self.make_donor(10, 10, 10, self.blood_types[i], i)
            self.insert_donor(json.dumps(donor))

        for i in range(8):
            response = self.app.delete(DONOR_BASE_PATH + str(i))
            self.assertEqual(response.status_code, 204)

            response = self.app.get(DONOR_BASE_PATH)
            self.assertEqual(len(response.json), 8 - (i + 1))

    def test_donor_update(self):
        old_donor = self.make_donor(10, 10, 10, 'AB+', 1)
        self.insert_donor(json.dumps(old_donor))

        inserted_donor = self.get_donor(1)
        self.assertEqual(old_donor, inserted_donor)

        new_donor = self.make_donor(20, 20, 20, 'AB-', 1)
        response = self.app.put(
            DONOR_BASE_PATH + str(1),
            data=json.dumps(new_donor),
            content_type='application/json')

        self.assertEqual(response.status_code, 204)
        updated_donor = self.get_donor(1)
        self.assertEqual(updated_donor, new_donor)

    def test_search_donor_by_blood(self):
        donor1 = self.make_donor(10, 10, 10, 'AB+', 1)
        donor2 = self.make_donor(10, 10, 10, 'AB+', 2)
        donor3 = self.make_donor(10, 10, 10, 'AB-', 3)
        self.insert_donor(json.dumps(donor1))
        self.insert_donor(json.dumps(donor2))
        self.insert_donor(json.dumps(donor3))

        query_string = {'blood': 'AB', 'rh': 'plus', 'page': 1, 'limit': 30}
        response = self.app.get(DONOR_BASE_PATH, query_string=query_string)

        self.assertEqual(len(response.json), 2)
        self.assertIn(donor1, response.json)
        self.assertIn(donor2, response.json)

        query_string = {'blood': 'AB', 'rh': 'minus'}
        response = self.app.get(DONOR_BASE_PATH, query_string=query_string)

        self.assertEqual(len(response.json), 1)
        self.assertIn(donor3, response.json)

        query_string = {'blood': 'O', 'rh': 'minus'}
        response = self.app.get(DONOR_BASE_PATH, query_string=query_string)
        self.assertEqual(len(response.json), 0)


if __name__ == '__main__':
    unittest.main()
