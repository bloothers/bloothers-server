import json
from flask import Response
from bloothers import create_app, db

DONOR_BASE_PATH = '/donors/'


class JsonResponse(Response):
    @property
    def json(self):
        return json.loads(self.get_data(as_text=True))


class InstrumentedTest:

    blood_types = ['AB+', 'AB-', 'A+', 'A-', 'B+', 'B-', 'O+', 'O-']

    def setUpIntegrationTests(self):
        self.flask = create_app(__name__, 'bloothers.config.Test')
        self.flask.response_class = JsonResponse
        self.clear_donors()
        self.app = self.flask.test_client()

    def make_donor(self,
                     latitude,
                     longitude,
                     donation_range,
                     blood_type,
                     donor_id=None,
                     last_donation=None):
        return {
            'latitude': float(latitude),
            'longitude': float(longitude),
            'donation_range': float(donation_range),
            'blood_type': blood_type,
            'last_donation': last_donation,
            'donor_id': donor_id
        }

    def insert_donor(self, donor):
        response = self.app.post(
            DONOR_BASE_PATH, data=donor, content_type='application/json')
        return response.json

    def get_donor(self, id):
        response = self.app.get(DONOR_BASE_PATH + str(id))
        return response.json

    def clear_donors(self):
        with self.flask.app_context():
            db.graph.run("MATCH (n:Donor) DETACH DELETE n")
