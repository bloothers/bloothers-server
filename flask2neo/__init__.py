from py2neo import Graph
from flask import current_app

try:
    from flask import _app_ctx_stack as stack
except ImportError:
    from flask import _request_ctx_stack as stack


class Flask2Neo(object):
    def __init__(self, app=None):
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app):

        app.config.setdefault('NEO4J_HOST', 'bolt://localhost:7687')
        app.config.setdefault('NEO4J_USER', 'neo4j')
        app.config.setdefault('NEO4J_PASSWORD', 'neo4j')

        if hasattr(app, 'teardown_appcontext'):
            app.teardown_appcontext(self.teardown)
        else:
            app.teardown_request(self.teardown)

    def connect(self):
        return Graph(
            current_app.config['NEO4J_HOST'],
            user=current_app.config['NEO4J_USER'],
            password=current_app.config['NEO4J_PASSWORD'])

    def teardown(self, exception):
        ctx = stack.top
        if hasattr(ctx, 'graph_db'):
            ctx.graph_db = None

    @property
    def graph(self):
        ctx = stack.top
        if ctx is not None:
            if not hasattr(ctx, 'graph_db'):
                ctx.graph_db = self.connect()
            return ctx.graph_db
