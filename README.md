# Bloothers Server &middot; [![pipeline status](https://gitlab.com/bloothers/bloothers-server/badges/master/pipeline.svg)](https://gitlab.com/bloothers/bloothers-server/commits/master)

Bloothers Server is the Bloothers REST API that performs all data manipulation for Donation control.

## Running Locally

Make sure you have [Python 3 installed](https://www.python.org/downloads/). Also you need to install and run the [Neo4j](https://neo4j.com/download/) server and [initialize the graph database with the BloodTypes](https://gitlab.com/bloothers/bloothers-tools/blob/master/graph/create_blood_types.cql).

```sh
$ git clone https://gitlab.com/bloothers/bloothers-server
$ cd bloothers-server
$ pip install -r requirements.txt
$ python run.py
```
The server will start on [localhost:5000](http://localhost:5000).
You can also use a [virtualenv](https://virtualenv.pypa.io/en/stable/) to config the environment and create environment variables to secret and personal values as database user and password. See the `bloothers/config/neo4j.py` to check the variables names. You can create a configuration file for flask based on one class from `bloothers/config/flask.py` and setting the path to the file on `BLOOTHERS_CONFIG` environment variable.

## Code linting and testing

The CI/CD server runs the tests and a linter to check if the code "smells" good. To check it locally you have to follow these steps:

```sh
$ pip install -r requirements-test.txt
$ python -m unittest -v # Will run all tests created.
$ pep8 bloothers/ # Will check if all the code follows the PEP8 rules.
```
For more details check the [pep8](http://pep8.readthedocs.io/en/release-1.7.x/) and [pytest](https://docs.pytest.org/en/latest/#documentation) documentation.

If the pep8 detects problems with the code, you can run:

```sh
$ yapf -r -i bloothers/ --style style.py
```
and/or fix the problems manually. Make sure the YAPF solves most PEP8 problems, but you will have to fix the strings and docstrings manually.
Another thing you can do is use our [vscode configurations](https://gitlab.com/bloothers/bloothers-tools/blob/master/configurations/vscode/settings.json) to help with linting.